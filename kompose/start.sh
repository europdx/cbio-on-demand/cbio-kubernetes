#!/bin/bash

git clone --depth 1 https://gitlab.ics.muni.cz/europdx-public/europdx-data-public inject_data
docker-compose up -d
for i in $(seq 10)
do
  if $(curl --output /dev/null --silent --head --fail http://localhost:8080)
  then
    echo "Server is up, breaking..."
    break
  else
    printf '.'
    sleep 5
  fi
done

docker exec -it cbio metaImport.py -u http://localhost:8080/cbioportal -s /inject_data/UNITO_dataset_20170626_curated -o
